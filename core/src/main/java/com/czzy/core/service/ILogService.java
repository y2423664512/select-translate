package com.czzy.core.service;

import com.czzy.core.entities.Log;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XD
 * @since 2021-11-16
 */
public interface ILogService extends IService<Log> {

}
