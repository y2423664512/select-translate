## 🐶 新手必读

* 演示地址：<http://select.yangliu233.com>
* 管理员账号：admin
* 管理员密码：admin

## 🐯 平台简介

自己以前花了两天时间写的一个小Demo，没想到还真的有人看，点赞(start)破20，我会将此代码重构，代码更加通俗易懂~，请大家多多点赞~
> 😜 给项目点点 Star 吧
> 

## 🐷 演示图

![架构图](./img/1.png)
![架构图](./img/2.png)
![架构图](./img/3.png)
![架构图](./img/4.png)
![架构图](./img/5.png)
![架构图](./img/6.png)