/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : localhost:3306
 Source Schema         : selectseat

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 28/11/2021 17:38:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `uid` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作员id',
  `object` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作事项',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `method_time` datetime(0) NULL DEFAULT NULL COMMENT '方法执行时间',
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作浏览器',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(2555) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布人id',
  `read_num` int(11) NULL DEFAULT 0 COMMENT '阅读次数',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1464850908920721410', '聚三心绘三色 滁州职业技术学院擦亮校地共建服务品牌', '滁州职业技术学院团委紧扣社会志愿服务主题，充分发挥学校青年志愿服务队优势，联合驻地周边志愿服务力量，组织12个志愿服务队与4个街道的12个社区开展结对共建、品牌共创活动，聚焦打造校地共建志愿服务品牌，突出凝聚青年学生忠心情怀、爱心情愫，切实在志愿服务中受教育、长才干、做贡献。\n聚忠心，共绘红色魂。切实利用好街道社区和人民群众的丰富党性教育资源，精准抓好党史学习教育。该校电气工程学院“守护星星”志愿者服务队通过寻访身边老党员的方式记录建党百年发展历程中的点点滴滴，引领团学青年在实践中学党史、强信念、跟党走，增强奋斗意识；食环学院“萤火虫”志愿服务队前往琅琊区三岔路社区党章主题教育馆开展“听老兵故事 忆党史初心”主题党团日活动学习党章的历史沿革。', '1464847968499675138', 0, '2021-11-28 14:57:27', '2021-11-28 14:57:27', 0);

-- ----------------------------
-- Table structure for tb_class
-- ----------------------------
DROP TABLE IF EXISTS `tb_class`;
CREATE TABLE `tb_class`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '教室名称',
  `total_num` int(11) NULL DEFAULT NULL COMMENT '容纳人数',
  `checked_num` int(11) NULL DEFAULT 0 COMMENT '已选人数',
  `is_open` tinyint(1) NULL DEFAULT NULL COMMENT '是否开启',
  `rows` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '排数',
  `open_time` datetime(0) NULL DEFAULT NULL COMMENT '开放时间',
  `close_time` datetime(0) NULL DEFAULT NULL COMMENT '关闭时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_class
-- ----------------------------
INSERT INTO `tb_class` VALUES ('5a291a17-ff47-48fa-8680-a421567128a0', '自习教室', 50, 0, 1, '0', '2021-11-16 00:00:00', '2021-12-07 00:00:00', '2021-11-28 15:00:55', '2021-11-28 15:00:55', 0);
INSERT INTO `tb_class` VALUES ('df0eed50-f101-4b43-babf-e5246b8e9f5a', '阶梯教室', 11, 0, 1, '0', '2021-11-16 00:00:00', '2021-12-07 00:00:00', '2021-11-28 15:00:42', '2021-11-28 15:00:42', 0);

-- ----------------------------
-- Table structure for tb_class_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_class_user`;
CREATE TABLE `tb_class_user`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `class_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '教室id',
  `seat_num` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '座位号',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_class_user
-- ----------------------------
INSERT INTO `tb_class_user` VALUES ('1464806795672330241', '1461221422975221762', '0618fa78-52fe-47e8-8044-2668503cd1e5', '1463689225640660995', '2021-11-28 12:02:10', '2021-11-28 12:02:10', 1);
INSERT INTO `tb_class_user` VALUES ('1464806907999985665', '1461221422975221762', '0618fa78-52fe-47e8-8044-2668503cd1e5', '1463689225640661004', '2021-11-28 12:02:36', '2021-11-28 12:02:36', 1);
INSERT INTO `tb_class_user` VALUES ('1464807180927541250', '1461221422975221762', '0618fa78-52fe-47e8-8044-2668503cd1e5', '1463689225640660999', '2021-11-28 12:03:41', '2021-11-28 12:03:41', 1);
INSERT INTO `tb_class_user` VALUES ('1464834056626667521', '1461221422975221762', '0618fa78-52fe-47e8-8044-2668503cd1e5', '1463689225640661001', '2021-11-28 13:50:29', '2021-11-28 13:50:29', 1);
INSERT INTO `tb_class_user` VALUES ('1464851812445106177', '1464847968499675138', '5a291a17-ff47-48fa-8680-a421567128a0', '1464851779796643877', '2021-11-28 15:01:02', '2021-11-28 15:01:02', 1);
INSERT INTO `tb_class_user` VALUES ('1464851841402580993', '1464847968499675138', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', '1464851727636279304', '2021-11-28 15:01:09', '2021-11-28 15:01:09', 1);

-- ----------------------------
-- Table structure for tb_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_message`;
CREATE TABLE `tb_message`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `uid` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '留言内容',
  `replay` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `rid` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复人id',
  `replay_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_message
-- ----------------------------
INSERT INTO `tb_message` VALUES ('1464852053105881090', '1464847968499675138', '阶梯教室的空调坏了，麻烦维修师傅来处理一下~', NULL, NULL, NULL, '2021-11-28 15:02:00', '2021-11-28 15:02:00', 0);

-- ----------------------------
-- Table structure for tb_seat
-- ----------------------------
DROP TABLE IF EXISTS `tb_seat`;
CREATE TABLE `tb_seat`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `cid` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图书馆教室id',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '座位状态(0未选,1已选)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_seat
-- ----------------------------
INSERT INTO `tb_seat` VALUES ('1464851727636279297', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279298', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279299', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279300', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279301', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279302', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279303', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279304', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279305', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279306', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851727636279307', 'df0eed50-f101-4b43-babf-e5246b8e9f5a', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534977', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534978', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534979', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534980', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534981', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534982', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534983', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534984', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534985', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534986', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534987', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534988', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534989', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779729534990', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643842', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643843', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643844', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643845', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643846', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643847', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643848', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643849', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643850', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643851', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643852', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643853', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643854', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643855', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643856', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643857', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643858', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643859', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643860', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643861', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643862', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643863', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643864', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643865', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643866', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643867', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643868', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643869', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643870', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643871', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643872', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643873', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643874', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643875', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643876', '5a291a17-ff47-48fa-8680-a421567128a0', 0);
INSERT INTO `tb_seat` VALUES ('1464851779796643877', '5a291a17-ff47-48fa-8680-a421567128a0', 0);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色',
  `head_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1464847210953846786', 'a', 'YQ==', 'a', 'user', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoM6WDpQlUMDAxqvq3iaXsx9OlqFKAcWZ7w5hbLb5fKqvcWYPCxTtydelytE6740jicEFAP3iaCAU6Ag/132', '2021-11-28 14:42:45', '2021-11-28 14:42:45', 0);
INSERT INTO `tb_user` VALUES ('1464847968499675138', 'admin', 'YWRtaW4=', 'admin', 'admin', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoM6WDpQlUMDAxqvq3iaXsx9OlqFKAcWZ7w5hbLb5fKqvcWYPCxTtydelytE6740jicEFAP3iaCAU6Ag/132', '2021-11-28 14:45:46', '2021-11-28 14:45:46', 0);

SET FOREIGN_KEY_CHECKS = 1;
